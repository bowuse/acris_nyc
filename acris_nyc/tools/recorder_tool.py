import re
from datetime import datetime, timedelta


class DocParser:
    def __init__(self, response):
        self.response = response
        self.xpath = response.xpath

    def get_data(self, item):
        search_url = 'https://a836-acris.nyc.gov/DS/DocumentSearch'

        doc_data = self.xpath("//font//text()").extract()
        doc_data = [i.strip() for i in doc_data]

        item['details_url'] = self.response.url
        item['doc_id'] = self.xpath(
            "//input[@name='hid_DocID']/@value").extract()[0]
        item['doc_type'] = self._get_doc_type(doc_data)
        item['doc_date'] = self._format_date(
            self._get_value(doc_data, "DOC. DATE:"))
        item['doc_amount'] = self._get_value(
            doc_data, 'DOC. AMOUNT:')
        item['record_date'] = self._format_date(
            self._get_value(doc_data, 'RECORDED / FILED:'))
        item['document_url'] = "{}/DocumentImageView?doc_id={}".format(
            search_url, item['doc_id'])

        return item

    def get_grantees(self, item):
        party1 = self.xpath("//div[@id='ABPOSITION']")[0]

        grantees = []
        names = (
            'grantee', 'grantee_address_1',
            'grantee_address_2', 'grantee_city',
            'grantee_state', 'grantee_zip',
            'grantee_country'
            )

        # grantee
        gr = party1.xpath(".//td[@width='15%']//font/text()").extract()
        gr_addr_1 = party1.xpath(".//td[@width='18%']//font/text()").extract()
        gr_addr_2 = self._get_address_2(party1, len(gr))
        gr_city = party1.xpath(".//td[@width='11%']//font/text()").extract()
        gr_state = ['NY' for i in range(len(gr))]
        gr_zip = party1.xpath(".//td[@width='10%']//font/text()").extract()
        gr_country = party1.xpath(".//td[@width='16%']//font/text()").extract()

        _zip = zip(
            gr, gr_addr_1, gr_addr_2,
            gr_city, gr_state, gr_zip, gr_country
            )

        for i in _zip:
            grantee_dict = dict(zip(names, i))
            grantees.append(grantee_dict)

        item['grantees'] = grantees
        return item

    def get_grantors(self, item):
        party2 = self.xpath("//div[@id='ABPOSITION']")[1]

        grantors = []
        names = (
            'grantor', 'grantor_address_1',
            'grantor_address_2', 'grantor_city',
            'grantor_state', 'grantor_zip',
            'grantor_country'
            )

        # grantor
        gr = party2.xpath(
            ".//td[@width='15%']//font/text()").extract()
        gr_address_1 = party2.xpath(
            ".//td[@width='18%']//font/text()").extract()
        gr_address_2 = self._get_address_2(party2, len(gr))
        gr_city = party2.xpath(
            ".//td[@width='11%']//font/text()").extract()
        gr_state = ['NY' for i in range(len(gr))]
        gr_zip = party2.xpath(
            ".//td[@width='10%']//font/text()").extract()
        gr_country = party2.xpath(
            ".//td[@width='16%']//font/text()").extract()

        _zip = zip(
            gr, gr_address_1, gr_address_2,
            gr_city, gr_state, gr_zip, gr_country
            )

        for i in _zip:
            grantor_dict = dict(zip(names, i))
            grantors.append(grantor_dict)

        item['grantors'] = grantors
        return item

    def get_parcels_data(self, item):
        parcels = self.xpath("//div[@id='ABPOSITION']")[3]
        rows = parcels.xpath(".//tr")

        names = ('county', 'apn', 'block', 'lot', 'property_address', 'unit')
        counties = []
        blocks = []
        lots = []
        apns = []
        addresses = []
        units = []

        for row in rows:
            tds = row.xpath("./td")
            county = tds[0].xpath(".//font/text()").extract_first()
            block = tds[1].xpath(".//font/text()").extract_first()
            lot = tds[2].xpath(".//font/text()").extract_first()
            address = tds[8].xpath(".//font/text()").extract_first()
            unit = tds[9].xpath(".//font/text()").extract_first()

            if county:
                if "MANHATTAN" in county:
                    county = "NEW YORK"
                elif "BROOKLYN" in county:
                    county = "KINGS"
                elif "STATEN ISLAND" in county:
                    county = "RICHMOND"
                else:
                    county = county.strip()

                block = block.strip()
                lot = lot.strip()
                apn = "-".join([block, lot])
                address = address.strip()
                unit = unit.strip()

                counties.append(county)
                blocks.append(block)
                lots.append(lot)
                apns.append(apn)
                addresses.append(address)
                units.append(unit)

        parcels = []
        for i in zip(counties, apns, blocks, lots, addresses, units):
            parc_dict = dict(zip(names, i))
            parcels.append(parc_dict)

        item['parcels'] = parcels

        return item

    def _format_date(self, date):
        d = re.search(r"\d+\/\d+\/\d+", date)
        if d:
            fdate = datetime.strptime(d[0], "%m/%d/%Y")
            return fdate.strftime("%Y-%m-%d")

        return ""

    def _get_value(self, data, name):
        return data[data.index(name) + 1]

    def _get_doc_type(self, data):
        doctype_ind = data.index('DOC. TYPE:')
        filenum_ind = data.index('FILE NUMBER:')
        doc_types = [data[i] for i in range(doctype_ind + 1, filenum_ind - 1)]
        return " ".join(doc_types)

    def _get_address_2(self, party, amount):
        tds = party.xpath(".//td[@width='19%']")
        addresses2 = [""] * amount

        for i in range(amount):
            addr = tds[i].xpath(".//font/text()").extract_first()
            if addr:
                addresses2[i] = addr

        return addresses2


class RequestData:
    def __init__(self, response, days=5, from_date=None, to_date=None):
        self.response = response
        self.xpath = response.xpath
        self.days = days
        self.from_date = from_date
        self.to_date = to_date

    def get_data(self, page=1):
        token = self.xpath(
            "//input[@name='__RequestVerificationToken']/@value").extract()[0]

        data = {
            "__RequestVerificationToken": token,
            "hid_doctype": "",
            "hid_doctype_name": "",
            "hid_selectdate": "DR",
            "hid_datefromm": "",
            "hid_datefromd": "",
            "hid_datefromy": "",
            "hid_datetom": "",
            "hid_datetod": "",
            "hid_datetoy": "",
            "hid_borough": "0",
            "hid_borough_name": "ALL BOROUGHS",
            "hid_max_rows": "999",
            "hid_page": str(page),
            "hid_ReqID": "",
            "hid_SearchType": "DOCTYPE",
            "hid_ISIntranet": "N",
            "hid_sort": ""
            }

        if self.from_date and self.to_date:
            date_from = datetime.strptime(self.from_date, "%Y-%m-%d")
            date_to = datetime.strptime(self.to_date, "%Y-%m-%d")
            date_range = date_to - date_from

            if date_range > timedelta(days=30):
                while date_from < date_to:
                    data['hid_datefromm'] = str(date_from.month)
                    data['hid_datefromd'] = str(date_from.day)
                    data['hid_datefromy'] = str(date_from.year)
                    date_from += timedelta(days=30)
                    if date_from > date_to:
                        data['hid_datetom'] = str(date_to.month)
                        data['hid_datetod'] = str(date_to.day)
                        data['hid_datetoy'] = str(date_to.year)
                    else:
                        data['hid_datetom'] = str(date_from.month)
                        data['hid_datetod'] = str(date_from.day)
                        data['hid_datetoy'] = str(date_from.year)
            else:
                data['hid_datefromm'] = str(date_from.month)
                data['hid_datefromd'] = str(date_from.day)
                data['hid_datefromy'] = str(date_from.year)
                data['hid_datetom'] = str(date_to.month)
                data['hid_datetod'] = str(date_to.day)
                data['hid_datetoy'] = str(date_to.year)

        else:
            data['hid_selectdate'] = str(self.days)

        yield data


class IDParser:
    def __init__(self, response):
        self.response = response
        self.xpath = response.xpath
        self.search_url = 'https://a836-acris.nyc.gov/DS/DocumentSearch'

    def get_ids(self):
        ids = re.findall(r'JavaScript:go_detail\("\d+', self.response.text)
        ids = [i.replace('JavaScript:go_detail("', '') for i in ids]
        return ids

    def get_url(self, _id):
        url = "{}/DocumentDetail?doc_id={}".format(
            self.search_url, _id)
        return url

    def get_max_rows(self):
        max_rows = self.xpath(
            "//input[@name='hid_max_rows']/@value").extract()[0]
        return max_rows

    def get_form_url(self, page):
        url = "{}/DocumentTypeResult?page={}".format(
            self.search_url, page)
        return url

    def get_hid_page(self):
        page = self.xpath(
            "//input[@name='hid_page']/@value").extract()[0]
        return int(page)

    def get_hid_doctype(self):
        doct = self.xpath(
            "//input[@name='hid_doctype']/@value").extract()[0]
        return doct

    def get_hid_doct_name(self):
        doct_name = self.xpath(
            "//input[@name='hid_doctype_name']/@value").extract()[0]
        return doct_name
