# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import FormRequest
from acris_nyc.tools.recorder_tool import RequestData
from acris_nyc.tools.recorder_tool import DocParser
from acris_nyc.tools.recorder_tool import IDParser


class NyNycRecorderSpider(scrapy.Spider):
    name = 'ny_nyc_recorder'

    allowed_domains = ['a836-acris.nyc.gov']
    start_urls = ['https://a836-acris.nyc.gov/DS/DocumentSearch/DocumentType']

    custom_settings = {
        'DOWNLOAD_DELAY': 2,
        'DEFAULT_REQUEST_HEADERS': {
            'Accept':
                'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Language': 'en',
            'User-Agent':
                'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'
            },
        'ROBOTSTXT_OBEY': False
    }

    document_types = (
        {'type': 'AMTX', 'name': 'ADDITIONAL MORTGAGE TAX'}, {'type': 'AGMT', 'name': 'AGREEMENT'}, {'type': 'AIRRIGHT', 'name': 'AIR RIGHTS'}, {'type': 'ADEC', 'name': 'AMENDED CONDO DECLARATION'}, {'type': 'AMFL', 'name': 'AMENDMENT OF FEDERAL LIEN'}, {'type': 'AMTL', 'name': 'AMENDMENT OF TAX LIEN'}, {'type': 'APPRT', 'name': 'APP. ORDER BREAKDWN OFFICE USE'}, {'type': 'XXXX', 'name': 'APPRT BREAKDWN OFFICE USE ONLY'}, {'type': 'AALR', 'name': 'ASGN OF ASGN OF L&R'}, {'type': 'ACON', 'name': 'ASSIGN/TERM OF CONTRACT/BID'}, {'type': 'ASSTO', 'name': 'ASSIGNMENT OF LEASE'}, {'type': 'AL&R', 'name': 'ASSIGNMENT OF LEASES AND RENTS'}, {'type': 'ATL', 'name': 'ASSIGNMENT OF TAX LIEN'}, {'type': 'ASST', 'name': 'ASSIGNMENT, MORTGAGE'}, {'type': 'ASPM', 'name': 'ASSUMPTION OF MORTGAGE'}, {'type': 'BOND', 'name': 'BOND'}, {'type': 'RPTT&RET', 'name': 'BOTH RPTT AND RETT'}, {'type': 'CALR', 'name': 'CANCEL/TERM ASGN L&R'}, {'type': 'NAFTL', 'name': 'CERT NONATTCHMENT FED TAX LIEN'}, {'type': 'CERT', 'name': 'CERTIFICATE'}, {'type': 'CERR', 'name': 'CERTIFICATE OF REDUCTION'}, {'type': 'WILL', 'name': 'CERTIFIED COPY OF WILL'}, {'type': 'CMTG', 'name': 'COLLATERAL MORTGAGE'}, {'type': 'CODP', 'name': 'CONDEMNATION PROCEEDINGS'}, {'type': 'CDEC', 'name': 'CONDO DECLARATION'}, {'type': 'CONDEED', 'name': 'CONFIRMATORY DEED'}, {'type': 'CONS', 'name': 'CONSENT'}, {'type': 'CNFL', 'name': 'CONTINUATION OF FEDERAL LIEN'}, {'type': 'CNTR', 'name': 'CONTRACT OF SALE'}, {'type': 'DEED COR', 'name': 'CORRECT  INDEX/DEED-OFFICE USE'}, {'type': 'CORR, LE', 'name': 'CORRECT LIFE ESTATE OFFICE USE'}, {'type': 'CORRD', 'name': 'CORRECTION DEED'}, {'type': 'CORR', 'name': 'CORRECTION DOC-OFFICE USE ONLY'}, {'type': 'CORRM', 'name': 'CORRECTION MORTGAGE'}, {'type': 'CTOR', 'name': 'COURT ORDER'}, {'type': 'DCTO', 'name': 'COURT ORDER ADVERSE POSS.'}, {'type': 'DECL', 'name': 'DECLARATION'}, {'type': 'DECM', 'name': 'DECLARATION OF MERGER'}, {'type': 'DEMM', 'name': 'DECLARATION OF MODIFI OF MRT'}, {'type': 'DEED', 'name': 'DEED'}, {'type': 'DEED, RC', 'name': 'DEED WITH RESTRICTIVE COVENANT'}, {'type': 'DEEDO', 'name': 'DEED, OTHER'}, {'type': 'DEEDP', 'name': 'DEED, PRE RPT TAX'}, {'type': 'DEVR', 'name': 'DEVELOPMENT RIGHTS'}, {'type': 'DPFTL', 'name': 'DISCHARGE OF PROPERTY FROM FTL'}, {'type': 'DTL', 'name': 'DISCHARGE OF TAX LIEN'}, {'type': 'EASE', 'name': 'EASEMENT'}, {'type': 'ESRM', 'name': 'ESTOPPAL REMOVAL OFFICE USE ON'}, {'type': 'ESTL', 'name': 'ESTOPPEL FOR OFFICE USE ONLY'}, {'type': 'FTL', 'name': 'FEDERAL LIEN, OTHER'}, {'type': 'FL', 'name': 'FEDERAL LIEN-IRS'}, {'type': 'IDED', 'name': 'IN REM DEED'}, {'type': 'INIC', 'name': 'INITIAL COOP UCC1'}, {'type': 'INIT', 'name': 'INITIAL UCC1'}, {'type': 'JUDG', 'name': 'JUDGMENT'}, {'type': 'LDMK', 'name': 'LANDMARK DESIGNATION'}, {'type': 'LEAS', 'name': 'LEASE'}, {'type': 'LTPA', 'name': 'LETTERS  PATENT'}, {'type': 'LIC', 'name': 'LICENSE'}, {'type': 'LOCC', 'name': 'LIEN OF COMMON CHARGES'}, {'type': 'DEED, LE', 'name': 'LIFE ESTATE DEED'}, {'type': 'MAPS', 'name': 'MAPS'}, {'type': 'MMTG', 'name': 'MASTER MORTGAGE'}, {'type': 'MCON', 'name': 'MEMORANDUM OF CONTRACT'}, {'type': 'MLEA', 'name': 'MEMORANDUM OF LEASE'}, {'type': 'MERG', 'name': 'MERGER'}, {'type': 'MISC', 'name': 'MISCELLANEOUS'}, {'type': 'MTGE', 'name': 'MORTGAGE'}, {'type': 'M&CON', 'name': 'MORTGAGE AND CONSOLIDATION'}, {'type': 'SPRD', 'name': 'MORTGAGE SPREADER AGREEMENT'}, {'type': 'NTXL', 'name': 'NOTICE OF  ESTATE TAX LIEN'}, {'type': 'NAPP', 'name': 'NOTICE OF APPROPRIATION'}, {'type': 'RPTT', 'name': 'NYC REAL PROPERTY TRANSFER TAX'}, {'type': 'RETT', 'name': 'NYS REAL ESTATE TRANSFER TAX'}, {'type': 'PRFL', 'name': 'PARTIAL RELEASE OF FED LIEN'}, {'type': 'PREL', 'name': 'PARTIAL RELEASE OF MORTGAGE'}, {'type': 'PRCFL', 'name': 'PARTIAL REVOCATION OF CERT RFL'}, {'type': 'PSAT', 'name': 'PARTIAL SATISFACTION'}, {'type': 'PWFL', 'name': 'PARTIAL WITHDRAWL OF FED LIEN'}, {'type': 'PAT', 'name': 'POWER OF ATTORNEY'}, {'type': 'REIT', 'name': 'REAL ESTATE INV TRUST DEED'}, {'type': 'REL', 'name': 'RELEASE'}, {'type': 'RTXL', 'name': 'RELEASE OF ESTATE TAX LIEN'}, {'type': 'RFL', 'name': 'RELEASE OF FEDERAL LIEN'}, {'type': 'RFTL', 'name': 'RELEASE OF FEDERAL TAX LIEN'}, {'type': 'RESO', 'name': 'RESOLUTION'}, {'type': 'RCRFL', 'name': 'REVOCATION OF CERTIF. OF RFL'}, {'type': 'RPAT', 'name': 'REVOCATION OF POWER OF ATTORNE'}, {'type': 'SAT', 'name': 'SATISFACTION OF MORTGAGE'}, {'type': 'SI CORR', 'name': 'SI BILLING UPDATE OFFICE USE'}, {'type': 'STP', 'name': 'STREET PROCEDURE'}, {'type': 'SUBL', 'name': 'SUBORDINATION OF LEASE'}, {'type': 'SUBM', 'name': 'SUBORDINATION OF MORTGAGE'}, {'type': 'SAGE', 'name': 'SUNDRY AGREEMENT'}, {'type': 'SMIS', 'name': 'SUNDRY MISCELLANEOUS'}, {'type': 'SMTG', 'name': 'SUNDRY MORTGAGE'}, {'type': 'TLS', 'name': 'TAX LIEN SALE CERTIFICATE'}, {'type': 'TOLCC', 'name': 'TERM OF LIEN OF COMMON CHARGES'}, {'type': 'TERDECL', 'name': 'TERM. OF CONDO DECLARATION'}, {'type': 'TERA', 'name': 'TERMINATION OF AGREEMENT'}, {'type': 'TL&R', 'name': 'TERMINATION OF ASSIGN OF L&R'}, {'type': 'TERL', 'name': 'TERMINATION OF LEASE OR MEMO'}, {'type': 'TERT', 'name': 'TERMINATION OF TRUST'}, {'type': 'DEED, TS', 'name': 'TIMESHARE DEED'}, {'type': 'TORREN', 'name': 'TORREN'}, {'type': 'CORP', 'name': 'UCC 5 CORRECTION STATEMENT'}, {'type': 'UCC ADEN', 'name': 'UCC COOPERATIVE ADDENDUM'}, {'type': 'AMND', 'name': 'UCC3 AMENDMENT'}, {'type': 'ASGN', 'name': 'UCC3 ASSIGNMENT'}, {'type': 'ASUM', 'name': 'UCC3 ASSUMPTION'}, {'type': 'BRUP', 'name': 'UCC3 BANKRUPTCY'}, {'type': 'CONT', 'name': 'UCC3 CONTINUATION'}, {'type': 'PSGN', 'name': 'UCC3 PARTIAL ASSIGNMENT'}, {'type': 'RLSE', 'name': 'UCC3 RELEASE/UCC AMENDMENT'}, {'type': 'SUBO', 'name': 'UCC3 SUBORDINATION'}, {'type': 'TERM', 'name': 'UCC3 TERMINATION'}, {'type': 'UCC1', 'name': 'UNIFORM COMMERCIAL CODE 1'}, {'type': 'UCC3', 'name': 'UNIFORM COMMERCIAL CODE 3'}, {'type': 'ASTU', 'name': 'UNIT ASSIGNMENT'}, {'type': 'VAC', 'name': 'VACATE ORDER'}, {'type': 'WFL', 'name': 'WITHDRAWAL OF A FED LIEN'}, {'type': 'WSAT', 'name': 'WITHHELD SATISFACTION'}, {'type': 'ZONE', 'name': 'ZONING LOT name'}
        )

    # find in date range: today-days to today
    days = None
    # find in date range: from_date to to_date
    # dates insert in format '%Y-%m-%d'
    from_date = None
    to_date = None

    def parse(self, response):
        search_url = 'https://a836-acris.nyc.gov/DS/DocumentSearch'

        request_data = RequestData(
                response=response,
                days=self.days,
                from_date=self.from_date,
                to_date=self.to_date
            )

        for data in request_data.get_data():
            for doc in self.document_types:
                data['hid_doctype'] = doc['type']
                data['hid_doctype_name'] = doc['name']

                yield FormRequest(
                    "{}/DocumentTypeResult?page=1".format(search_url),
                    formdata=data,
                    callback=self.parse_ids)

    def parse_ids(self, response):
        parser = IDParser(response)
        ids = parser.get_ids()

        if len(ids) > 0:
            for _id in ids:
                req_url = parser.get_url(_id)
                yield scrapy.Request(
                    url=req_url,
                    callback=self.parse_doc)

        max_rows = parser.get_max_rows()
        if len(ids) == int(max_rows):
            request_data = RequestData(
                response=response,
                days=self.days,
                from_date=self.from_date,
                to_date=self.to_date
            )
            hid_page = parser.get_hid_page()
            hid_doctype = parser.get_hid_doctype()
            hid_doctype_name = parser.get_hid_doct_name()

            for data in request_data.get_data(page=hid_page + 1):
                data['hid_doctype'] = hid_doctype
                data['hid_doctype_name'] = hid_doctype_name
                form_url = parser.get_form_url(page=hid_page + 1)
                yield FormRequest(
                        url=form_url,
                        formdata=data,
                        callback=self.parse_ids)

    def parse_doc(self, response):
        if 'DocumentDetail' in response.url:
            item = {}
            doc_parser = DocParser(response)

            item = doc_parser.get_data(item)
            item = doc_parser.get_grantees(item)
            item = doc_parser.get_grantors(item)
            item = doc_parser.get_parcels_data(item)

            return item
