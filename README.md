# acris_nyc

Scrapy project that crawls property records from [a836-acris.nyc.gov](https://a836-acris.nyc.gov/CP)

## Setup
Requires **Python3** and **Scrapy1.5** installed.
Install requirements.
	
	pip install -r requirements.txt
	
## Usage

#### By default spider crawls last 5 days
	
	scrapy crawl ny_nyc_recorder

#### Crawl documents in specific date range 

	scrapy crawl ny_nyc_recorder -a from_date=YYYY-MM-DD -a to_date=YYYY-MM-DD
	
#### Crawl documents for N last days

	scrapy crawl ny_nyc_recorder -a days=N
	
## Extracted data
Example of extracted data in data_example.json
